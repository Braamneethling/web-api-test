﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTestApp.Models
{
    public class Photo
    {
        public string PhotoId { get; set; }
        public string PhotoUrl { get; set; }
        public string LocationId { get; set; }
    }
}