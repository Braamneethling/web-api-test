﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTestApp.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string LocationId { get; set; }
        public string LocationName { get; set; }
    }
}