﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTestApp.Models
{
    public class PhotoDetail
    {
        public string PhotoDescription { get; set; }
        public string PhotoAddress { get; set; }
    }
}