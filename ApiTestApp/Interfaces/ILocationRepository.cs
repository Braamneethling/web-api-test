﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiTestApp.Models;
using FoursquareApi.Models.Venues;

namespace ApiTestApp.Interfaces
{
    public interface ILocationRepository
    {
        void StoreLocations(Location location);
        List<Location> GetAllStoredLocations();
        void SavePhotosForLocations(LocationPhoto photo);
        List<LocationPhoto> GetAllSavedPhotosForLocations();
    }
}
