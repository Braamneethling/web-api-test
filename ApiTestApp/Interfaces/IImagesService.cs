﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiTestApp.Models;
using FoursquareApi;
using FoursquareApi.Models;
using FoursquareApi.Models.Common;
using FoursquareApi.Models.Common.Photo;
using FoursquareApi.Models.Common.Venue;
using FoursquareApi.Models.Venues;

namespace ApiTestApp.Interfaces
{
    public interface IImagesService
    {
        List<LocationPhoto> FindAneSaveResultsForPhotoSearch(Foursquare foursquare,
            List<string> locations);
        void SaveLocations(List<CompactVenue> venues);
    }
}
