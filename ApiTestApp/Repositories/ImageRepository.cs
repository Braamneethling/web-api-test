﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ApiTestApp.Interfaces;
using ApiTestApp.Models;
using FoursquareApi.Models.Venues;
using RepoDb;
using RepoDb.Enumerations;
using RepoDb.Interfaces;

namespace ApiTestApp.Repositories
{
    public class ImageRepository : BaseRepository<Location, SqlConnection>, ILocationRepository
    {
        public ImageRepository() : base(ConfigurationManager.AppSettings["connectionString"])
        {
        }

        public void StoreLocations(Location location)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Insert<Location>("[dbo].[Locations]", entity: location);
            }
        }

        public List<Location> GetAllStoredLocations()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                return connection.QueryAll<Location>("[dbo].[Locations]").ToList();
            }
        }

        public void SavePhotosForLocations(LocationPhoto locationPhoto)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Insert<LocationPhoto>("[dbo].[LocationPhotos]", entity: locationPhoto);
            }
        }

        public List<LocationPhoto> GetAllSavedPhotosForLocations()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                return connection.QueryAll<LocationPhoto>("[dbo].[LocationPhotos]").ToList();
            }
        }
    }
}