﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Compilation;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using System.Web.WebPages;
using ApiTestApp.Areas.HelpPage.Models;
using ApiTestApp.Interfaces;
using ApiTestApp.Models;
using FoursquareApi;
using FoursquareApi.Models.Common;
using FoursquareApi.Models.Common.Venue;
using FoursquareApi.Models.Venues;

namespace ApiTestApp.Controllers
{
    public class ImagesController : ApiController
    {
        private const string AppropriateCategories = "56aa371be4b08b9a8d5734db,4fceea171983d5d06c3e9823,4bf58dd8d48988d181941735,4bf58dd8d48988d184941735,52e81612bcbc57f1066b7a22";
        private static readonly string ClientId = ConfigurationManager.AppSettings["clientId"];
        private static readonly string ClientSecret = ConfigurationManager.AppSettings["clientSecret"];
        private static readonly string RedirectUrl = "/Home/Index.cshtml";
        private readonly IImagesService _imagesService;
        public ImagesController(IImagesService imagesService)
        {
            this._imagesService = imagesService;
        }

        /// <summary>
        /// Gets the 5 interesting locations close to specified location.
        /// </summary>
        /// <param name="location"></param>
        /// <returns>
        /// An list of venues/>
        /// </returns>
        [Route("api/Images/GetLocations"), HttpGet]
        public IHttpActionResult GetLocations(string location)
        {
            if (location.IsEmpty()) return BadRequest("Please specify a location");
            
            var foursquare = FoursquareBuilder();

            //I limited this because of the number of calls allowed for Free users on the Foursquare API
            var venues = foursquare.Venues.SearchVenues(location, categoryId: AppropriateCategories,limit:5).Response.Venues.ToList();
            if (!venues.Any())
            {
                return BadRequest("No Location landmarks found");
            }

            _imagesService.SaveLocations(venues);
            return Ok(venues);
        }

        /// <summary>
        /// Gets the photos for the returned locations.
        /// </summary>
        /// <param name="locations"></param>
        /// <returns>
        /// An list of photos for returned locations
        /// </returns>
        [Route("api/Images/GetPhotosForLocations"), HttpGet]
        public IHttpActionResult GetPhotosForLocations([FromUri]List<string> locations)
        {
            if (!locations.Any()) return BadRequest("No valid locations found to retrieve photo's for");
            var foursquare = FoursquareBuilder();
            
            var retrieveImagesForLocations = _imagesService.FindAneSaveResultsForPhotoSearch(foursquare, locations);

            if (retrieveImagesForLocations == null || !retrieveImagesForLocations.Any())
            {
                return BadRequest("No photos found for location");
            }

            return Ok(retrieveImagesForLocations);
        }

        /// <summary>
        /// Gets details for the photo clicked.
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns>
        /// Details for the requested venue photo
        /// </returns>
        [Route("api/Images/GetDetailsForPhoto"), HttpGet]
        public IHttpActionResult GetDetailsForPhoto(string locationId)
        {
            if (locationId.IsEmpty()) return BadRequest("Please specify a venueId");
            var foursquare = FoursquareBuilder();

            var venueDetailModel = foursquare.Venues.VenueDetail(locationId);
            
            if (venueDetailModel is null)
            {
                return BadRequest("No details found for venue");
            }

            var photoDetail = BuildPhotoDetailModel(venueDetailModel);

            return Ok(photoDetail);
        }

        private static PhotoDetail BuildPhotoDetailModel(VenueDetailModel venueDetailModel)
        {
            return new PhotoDetail
            {
                PhotoAddress = venueDetailModel.Response.Venue.Location.Address,
                PhotoDescription = venueDetailModel.Response.Venue.Description

            };
        }

        private static Foursquare FoursquareBuilder()
        {
            return new Foursquare(ClientId, ClientSecret, RedirectUrl, null);
        }
    }
}
