﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ApiTestApp.Interfaces;
using ApiTestApp.Models;
using FoursquareApi;
using FoursquareApi.Models;
using FoursquareApi.Models.Common.Photo;
using FoursquareApi.Models.Common.Venue;
using FoursquareApi.Models.Venues;

namespace ApiTestApp.Services
{
    public class ImagesService : IImagesService
    {
        private readonly ILocationRepository _locationRepository;

        public ImagesService(ILocationRepository locationRepository)
        {
            _locationRepository = locationRepository;
        }

        public List<LocationPhoto> FindAneSaveResultsForPhotoSearch(Foursquare foursquare,
            List<string> locations)
        {
            var photosModels = new List<LocationPhoto>();
            var allSavedPhotosForLocations = _locationRepository.GetAllSavedPhotosForLocations();

            foreach (var locationId in locations)
            {
                var venuePhotosModel = foursquare.Venues.VenuePhotos(locationId);
                if (venuePhotosModel is null)
                {
                    continue;
                }
                
                var foursquareItemsEntity = venuePhotosModel.Response.Photos.Items;
                foreach (var photo in foursquareItemsEntity)
                {
                    var photoModel = BuildPhotoModel(photo, locationId);
                    photosModels.Add(photoModel);
                    var locationPhoto = allSavedPhotosForLocations.Find(x => x.PhotoId == photo.Id);
                    if (IfLocationPhotoIsAlreadyInDatabase(locationPhoto)) continue;
                    _locationRepository.SavePhotosForLocations(photoModel);
                }
            }

            return photosModels;
        }
        
        public void SaveLocations(List<CompactVenue> venues)
        {
            var allLocations = _locationRepository.GetAllStoredLocations();
            foreach (var venue in venues)
            {
                var location = CheckIfLocationExistsInDb(allLocations, venue);
                if (location == null)
                {
                    _locationRepository.StoreLocations(BuildLocation(venue));
                }
            }
        }

        private static bool IfLocationPhotoIsAlreadyInDatabase(LocationPhoto locationPhoto)
        {
            return locationPhoto != null;
        }

        private static Location CheckIfLocationExistsInDb(List<Location> allLocations, CompactVenue venue)
        {
            return allLocations.Find(x => x.LocationId == venue.Id);
        }

        private static LocationPhoto BuildPhotoModel(Photo photo, string locationId)
        {
            return new LocationPhoto
            {
                PhotoId = photo.Id,
                LocationId = locationId,
                PhotoUrl = photo.Prefix + "200x200" + photo.Suffix

            };
        }

        private Location BuildLocation(CompactVenue venue)
        {
            return new Location
            {
                LocationId = venue.Id,
                LocationName = venue.Name
            };
        }
    }
}